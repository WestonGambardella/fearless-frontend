function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
    <div class="card shadow p-3 mb-5 bg-body-tertiary rounded grid gap-0 row-gap-3">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location} </h6>
        <p class="card-text">${description}</p>
        <div class="card-footer">
          ${starts} - ${ends}
        </div>
      </div>
    </div>
  `;
}

function createError() {
  return `
    <h1 class="alert alert-danger">
    Bad request!
    </h1>
  `
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const html2 = createError()
        const main = document.querySelector('main')
        main.innerHTML += html2
      } else {
        const data = await response.json();
        let counter = 0
        for (let conference of data.conferences) {
          counter += 1;
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const starts = new Date(details.conference.starts);
            const startsformat = starts.toLocaleDateString()
            const ends = new Date(details.conference.ends);
            const endsformat = ends.toLocaleDateString()
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, startsformat, endsformat, location);
            const column1 = document.querySelector('.col1');
            const column2 = document.querySelector('.col2');
            const column3 = document.querySelector('.col3');
            if (counter%3==1){
              column1.innerHTML += html;
            } else if (counter%3==2) {
              column2.innerHTML += html;
            } else {
              column3.innerHTML += html;
            }

          }
        }

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });
