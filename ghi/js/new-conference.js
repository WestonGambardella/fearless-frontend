window.addEventListener("DOMContentLoaded", async () => { //The DOMContentLoaded event fires when the HTML document has been completely parsed
const url = 'http://localhost:8000/api/locations/' // we're trying to create a dropdown menu for locations when creating a new conference, so we need the locations data to access name
try { //this just makes sure the url is working, if not then throw error(look at line 42)
    const response = await fetch(url); //need the await or else this will produce a promise object

    if(response.ok) { //execute the code below if the response is not ok
        const data = await response.json() //transfer data captured from url to JSON
        const selectTag = document.getElementById("location")
        //console.log(data)
        for (let location of data.locations){ //for each location
            const option = document.createElement("option"); //create a new element called "option", which will house each location name in drop down menu
            option.value = location.id //we attach an id in case we need to delete or switch location, we had to add id to the list location encoder to get access to it
            option.innerHTML = location.name //for the <option> tag that's created on line 11, put the location name in the tag
            selectTag.appendChild(option) //attach this option to the selectTag which is using the id="locaton" that's attached to a html tag on the html page
            } //end for loop, so this creates an option with the name being the location name for every location
            const formTag = document.getElementById("create-conference-form"); //now we're on to creating conference, so we need to get the html form element with the id="create-conference-form"
            formTag.addEventListener("submit", async event => { //this is "listening" for the user to submit the form, when it does:execute code below
                event.preventDefault(); //this makes it so the form can be submitted without having to refresh the entire page
                const formData = new FormData(formTag); //this is attaching submitted form data to var formData, while also transforming data to FormData(an "object-like" value, but not an obj)
                const json = JSON.stringify(Object.fromEntries(formData)); //this puts newly submitted data from formData to a string that can be interpreted by JSON

                const conferenceUrl = "http://localhost:8000/api/conferences/" //this is the url we will be posting the new information to
                const fetchConfig = { //this is the function we will be attaching  to fetch when we send the data back to the conferenceUrl
                    method: "POST", //set the method type, it's POST since we're creating a new conference
                    body: json, //this is the main part of the "payload", it;s the data from the form that was translated to FormData then to a JSON string
                    headers: {
                        "Content-Type": "application/json", //this is the header or the "payload", which will tell the conferenceUrl server that what's coming will be JSON
                    }
                }
                const response = await fetch(conferenceUrl, fetchConfig); //this is posting the form data to conferenceUrl server
                if(response.ok) { //if the post response is ok
                    formTag.reset() //reset the form so another conference can be created if need be
                    const newConference = await response.json(); //I don't understand why this is needed to be honest
                }

            })

        }
    }


catch (e) {
    //console.error(e);
  // Figure out what to do if an error is raised
}
})
