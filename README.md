
<h1>About the project</h1>
    <p>This application is aimed at connecting users with upcoming conferences around the United States. If you would like to host a conference, or browse around for one you would like to attend, this application handles all the functionality.</p>

<h1>Built With</h1>
    <ul>Postgres</ul>
    <ul>RabbitMQ</ul>
    <ul>Docker</ul>
    <ul>Django</ul>
    <ul>HTML5</ul>
    <ul>Bootstrap</ul>

<h1>Getting Started</h1>
<p>Just a few things before you dive into the project!</p>
    <h2>Prerequisites</h2>
    <ul>Docker Desktop</ul>
    <h2>Installation</h2>
    <ul>Clone the repository into a directory of your choosing</ul>
    <ul>Create an .env file in the main directory of project</ul>
    <ul>Get a free API key from Pexels here: https://help.pexels.com/hc/en-us/articles/900004904026-How-do-I-get-an-API-key-</ul>
    <ul>Get a free API key from OpenWeatherMap here: https://openweathermap.org/current</ul>
    <ul>Create two environment variables in .env file titled OPEN_WEATHER_API_KEY and PEXELS_API_KEY, and assign your API keys to both environment variables</ul>
    <ul>Use the "docker-compose build" command to build the necessary images</ul>
    <ul>Then use the "docker-compose up" command to create the containers</ul>
    <ul>Head over to http://localhost:3000/ to interact with the application!</ul>
    <h2>Usage</h2>
    <p>First thing would be to create a location. This is needed since every new conference must be tied to a location. Then, you are able to create a conference using the newly created location. After you create your conference, you should see a card on the main page displaying the conference you just created. Feel free to poke around and check out the functionality!</p>
